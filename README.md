# Insure Software Diversity Project


## Dispatch

User Program comes in, and it triggers the compiler based on user settings

Is a webserver written in Go.

Steps to run the webserver:

1. Build the "hello.go" file using the command - go build hello.go
2. Run the server - go run hello.go
(The server is now running)
3. Now open the goupload.html file in the browser and select a file to upload.
4. On submitting the file, the file would be save in the "/tmp" folder with the name "uploadfile". 